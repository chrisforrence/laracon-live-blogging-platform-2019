module.exports = {
  theme: {
    colors: {
      'white': '#FFFCFF',
      'dark-shades': '#211621',
      'dark-accent': '#B63D51',
      'main-brand': '#F85142',
      'light-accent': '#8190A5',
      'light-shades': '#F4F7F3'
    }
  },
  variants: {},
  plugins: []
}
