<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Presenter extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'title', 'twitter'];

    public function talks()
    {
        return $this->hasMany(Talk::class);
    }

    public function getSluggedNameAttribute()
    {
        return Str::slug($this->name);
    }
}
