<?php

namespace App\Console\Commands;

use App\Presenter;
use App\Talk;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CreateTalk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'talk:create {title} {--presenter=} {--start=} {--duration=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a talk in the system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $talk = Talk::create([
            'title' => $this->argument('title')
        ]);

        if ($this->hasOption('presenter')) {
            $presenter = Presenter::where('name', $this->option('presenter'))->first();
            if ($presenter) {
                $talk->presenter_id = $presenter->id;
                $talk->save();
            }
        }

        if ($this->hasOption('starts')) {
            $start = strtotime($this->option('start'));

            if ($start !== false) {
                $talk->starts_at = Carbon::parse($start);
                $talk->save();

                if ($this->hasOption('duration')) {
                    $talk->ends_at = Carbon::parse($start)->addMinutes($this->option('duration'));
                    $talk->save();
                }
            }
        }
    }
}
