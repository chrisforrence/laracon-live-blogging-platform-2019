<?php

namespace App\Console\Commands;

use App\Presenter;
use Illuminate\Console\Command;

class CreatePresenter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'presenter:create {name} {--twitter=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a presenter entry';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Presenter::create([
            'name' => $this->argument('name'),
            'twitter' => $this->option('twitter'),
        ]);
    }
}
