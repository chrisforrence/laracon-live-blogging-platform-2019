<?php

namespace App\Console\Commands;

use App\Events\TalkStarted;
use App\Talk;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CheckForNewTalk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'talks:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Checks if there's a new talk starting";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current = Cache::get('talks.current');
        $talk = Talk::current()->first();
        
        if ($talk === null) {
            // No currently ongoing talk.
            Log::debug('No current talk.');
            return true;
        }
        if ($current === null || $talk->id !== $current->id) {
            Log::info('New talk starting.');
            Cache::set('talks.current', $talk);
            broadcast(new TalkStarted());
        }
        return true;
    }
}
