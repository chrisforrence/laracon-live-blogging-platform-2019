<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Talk extends Model
{
    protected $fillable = ['title', 'starts_at', 'ends_at', 'slides_url', 'presenter_id'];

    protected $dates = ['starts_at', 'ends_at'];

    public function presenter()
    {
        return $this->belongsTo(Presenter::class)->withDefault([
            'name' => 'A Surprise, Guest Speaker',
        ]);
    }

    public function getSluggedTitleAttribute()
    {
        return Str::slug($this->title);
    }

    public function scopeCurrent($query)
    {
        $now = Carbon::now();
        return $query->where('starts_at', '<=', $now)
            ->where('ends_at', '>', $now);
    }

    public function scopePrevious($query)
    {
        return $query->where('ends_at', '<', Carbon::now())->orderBy('starts_at');
    }

    public function scopeUpcoming($query)
    {
        return $query->where('starts_at', '>', Carbon::now())->orderBy('starts_at');
    }

    public function isCurrent()
    {
        $now = Carbon::now();
        return $this->starts_at <= $now && $this->ends_at > $now;
    }

    public function isPrevious()
    {
        $now = Carbon::now();
        return $this->ends_at < $now;
    }

    public function isUpcoming()
    {
        $now = Carbon::now();
        return $this->starts_at > $now;
    }
}
