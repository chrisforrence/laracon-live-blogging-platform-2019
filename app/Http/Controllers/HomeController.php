<?php

namespace App\Http\Controllers;

use App\Talk;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $previous = Talk::previous()->get();
        $talk = Talk::current()->first();
        $upcoming = Talk::with('presenter')->upcoming()->first();

        return view('guest.welcome', compact('talk', 'upcoming', 'previous'));
    }
}
