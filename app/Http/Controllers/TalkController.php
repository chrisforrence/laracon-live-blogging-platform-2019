<?php

namespace App\Http\Controllers;

use App\Events\TalkUpdated;
use App\Http\Requests\CreateTalkRequest;
use App\Presenter;
use App\Talk;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TalkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $talks = Talk::with('presenter')->orderBy('starts_at')->get();
        return view((Auth::check() ? '' : 'guest.') . 'talks.index', compact('talks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $presenters = Presenter::orderBy('name')->get();
        return view('talks.create', compact('presenters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTalkRequest $request)
    {
        $talk = Talk::create([
            'title' => $request->get('title'),
            'presenter_id' => $request->get('presenter_id') ?: null,
            'starts_at' => (new Carbon($request->get('starts_at'), 'America/New_York'))
                ->setTimezone('UTC'),
            'ends_at' => (new Carbon($request->get('ends_at'), 'America/New_York'))
                ->setTimezone('UTC'),
            'slides_url' => $request->get('slides_url') ?: null,
        ]);
        return redirect()->route('talks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Talk  $talk
     * @return \Illuminate\Http\Response
     */
    public function show(Talk $talk)
    {
        $talk->load('presenter');
        return view((Auth::check() ? '' : 'guest.') . 'talks.show', compact('talk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Talk  $talk
     * @return \Illuminate\Http\Response
     */
    public function edit(Talk $talk)
    {
        $presenters = Presenter::orderBy('name')->get();
        return view('talks.edit', compact('talk', 'presenters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Talk                $talk
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, Talk $talk)
    {
        $talk->load('presenter');
        if ($request->has('title')) {
            $talk->title = $request->get('title');
        }

        if ($request->has('starts_at')) {
            $talk->starts_at = (new Carbon($request->get('starts_at'), 'America/New_York'))
                ->setTimezone('UTC');
        }

        if ($request->has('ends_at')) {
            $talk->ends_at = (new Carbon($request->get('ends_at'), 'America/New_York'))
                ->setTimezone('UTC');
        }

        if ($request->has('slides_url')) {
            $talk->slides_url = $request->get('slides_url') ?: null;
        }

        if ($request->has('contents')) {
            $talk->contents = $request->get('contents') ?: '';
        }

        if ($request->has('presenter')) {
            $presenter = Presenter::find($request->get('presenter'));
            $talk->presenter()->associate($presenter);
        }

        $talk->save();
        broadcast(new TalkUpdated($talk));
        return response('', 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Talk  $talk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Talk $talk)
    {
        //
    }
}
