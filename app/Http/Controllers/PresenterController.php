<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePresenterRequest;
use App\Http\Requests\UpdatePresenterRequest;
use App\Presenter;
use Illuminate\Http\Request;

class PresenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $presenters = Presenter::with('talks')->orderBy('name', 'ASC')->get();
        return view('presenters.index', compact('presenters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('presenters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePresenterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePresenterRequest $request)
    {
        Presenter::create($request->all());

        return redirect()->to('/presenters');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Presenter  $presenter
     * @return \Illuminate\Http\Response
     */
    public function show(Presenter $presenter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Presenter  $presenter
     * @return \Illuminate\Http\Response
     */
    public function edit(Presenter $presenter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePresenterRequest  $request
     * @param  \App\Presenter  $presenter
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePresenterRequest $request, Presenter $presenter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Presenter  $presenter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Presenter $presenter)
    {
        //
    }
}
