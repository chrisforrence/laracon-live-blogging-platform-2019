<?php

namespace App\Http\Controllers;

use App\Presenter;
use App\Talk;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard')
            ->with('talks', Talk::with('presenter')->orderBy('starts_at')->get())
            ->with('presenters', Presenter::with('talks')->orderBy('name')->get());
    }
}
