@extends('layouts.app')

@section('content')
    <div class="flex flex-col md:flex-row">

        <div class="w-full md:w-1/2 xl:w-1/3">
            <h2 class="text-2xl">Presenters:</h2>
            <a href="{{ route('presenters.create') }}">New Presenter...</a>
            <br>
            <ul>
                @forelse($presenters as $presenter)
                    <li>
                        <a href="{{ route('presenters.edit', [$presenter]) }}">
                            {{ $presenter->name }}
                        </a> with {{ $presenter->talks->count() }} {{ \Illuminate\Support\Str::plural('talk', $presenter->talks->count()) }}
                    </li>
                @empty
                    <li><em>No presenters...</em></li>
                @endforelse
            </ul>
        </div>
        <div class="w-full md:w-1/2 xl:w-1/3">
            <h2 class="text-2xl">Talks:</h2>
            <a href="{{ route('talks.create') }}">New Talk...</a>
            <br>
            <ul>
                @forelse($talks as $talk)
                    <li>
                        <a href="{{ route('talks.edit', [$talk]) }}">
                            {{ $talk->title }}
                        </a> by {{ $talk->presenter->name }}
                    </li>
                @empty
                    <li><em>No talks...</em></li>
                @endforelse
            </ul>
        </div>
    </div>
@endsection