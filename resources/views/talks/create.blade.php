@extends('layouts.app')

@section('content')
    <form method="post" action="{{ route('talks.store') }}">
        @csrf
        <header class="flex flex-col md:flex-row items-center justify-between mb-8">
            <div class="inline-block relative text-3xl w-1/2">
                <input type="text" id="title" name="title"
                       class="bg-white w-full text-3xl bg-white border border-light-shades hover:border-light-accent shadow"
                       placeholder="">
            </div>
            <div class="inline-block relative w-64">
                <select id="presenter" name="presenter_id"
                        class="block appearance-none text-xl uppercase tracking-wider w-full bg-white border border-light-shades hover:border-light-accent px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                    <option value="">Select A Presenter</option>
                    <option disabled>---</option>
                    @foreach ($presenters as $presenter)
                        <option value="{{ $presenter->id }}">
                            {{ $presenter->name }}
                        </option>
                    @endforeach
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-light-accent">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                </div>
            </div>
        </header>
        <section class="mb-8">

            <label for="starts_at" class="mr-8">Start/End: </label>
            <input type="datetime-local" id="starts_at" name="starts_at" value="{{ Carbon\Carbon::now()->addHour()->setMinutes(0)->setSeconds(0)->setTimezone('America/New_York')->toDateTimeLocalString() }}" />
            <input type="datetime-local" id="ends_at" name="ends_at" value="{{ Carbon\Carbon::now()->addHour()->setMinutes(30)->setSeconds(0)->setTimezone('America/New_York')->toDateTimeLocalString() }}" />
        </section>
        <section class="flex flex-col md:flex-row items-center justify-between mb-8">
            <label for="slides_url" class="mr-8">Slides: </label>
            <input type="url" id="slides_url" name="slides_url"
                   class="bg-white flex-1 bg-white border border-light-shades hover:border-light-accent shadow"
                   value="">
        </section>
        <section class="mb-8">
            <button type="submit" class="shadow bg-dark-accent text-light-shades focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="button">
                {{ __('Save') }}
            </button>
        </section>
    </form>
@endsection