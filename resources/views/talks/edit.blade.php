@extends('layouts.app')

@section('content')
    <form action="#">
        <header class="flex flex-col md:flex-row items-center justify-between mb-8">
            <div class="inline-block relative text-3xl w-1/2">
                <input type="text" id="title"
                       class="bg-white w-full text-3xl bg-white border border-light-shades hover:border-light-accent shadow"
                       value="{{ $talk->title }}">
            </div>
            <div class="inline-block relative w-64">
                <select id="presenter"
                        class="block appearance-none text-xl uppercase tracking-wider w-full bg-white border border-light-shades hover:border-light-accent px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                    <option>Select A Presenter</option>
                    <option disabled>---</option>
                    @foreach ($presenters as $presenter)
                        <option value="{{ $presenter->id }}" {{ $presenter->id === $talk->presenter->id ? 'selected ' : '' }}>
                            {{ $presenter->name }}
                        </option>
                    @endforeach
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-light-accent">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                </div>
            </div>
        </header>
        <section>
            <input type="datetime-local" id="starts_at" value="{{ $talk->starts_at->setTimezone('America/New_York')->toDateTimeLocalString() }}" />
            <input type="datetime-local" id="ends_at" value="{{ $talk->ends_at->setTimezone('America/New_York')->toDateTimeLocalString() }}" />
        </section>
        <section>
            <input type="url" id="slides_url"
                   class="bg-white w-full bg-white border border-light-shades hover:border-light-accent shadow"
                   value="{{ $talk->slides_url }}">
        </section>
        <main class="">
            <textarea class="w-full shadow rounded font-mono" name="contents" id="contents" cols="30"
                      rows="10">{{ $talk->contents }}</textarea>
        </main>
    </form>
@endsection

@push('scripts')
    <script>
        var counter = 0;
        {{--window.Echo.join('talk.{{ $talk->id }}')--}}
        {{--    .here((users) => {--}}
        {{--        counter = users.length;--}}
        {{--        updateCounter();--}}
        {{--    })--}}
        {{--    .joining((user) => {--}}
        {{--        counter++;--}}
        {{--        updateCounter();--}}
        {{--    })--}}
        {{--    .leaving((user) => {--}}
        {{--        counter--;--}}
        {{--        updateCounter();--}}
        {{--    });--}}

        function updateCounter() {
            document.getElementById('counter').innerText = counter;
        }

        function titleChanged() {
            console.log("title changed");
            window.axios.patch('/talks/{{ $talk->id }}', {
                title: document.getElementById('title').value
            }).then((response) => {
            });
        }
        function presenterChanged() {
            console.log("presenter changed");
            window.axios.patch('/talks/{{ $talk->id }}', {
                presenter: document.getElementById('presenter').value
            }).then((response) => {
            });
        }
        function startsAtChanged() {
            console.log("startsAt changed");
            window.axios.patch('/talks/{{ $talk->id }}', {
                starts_at: document.getElementById('starts_at').value
            }).then((response) => {
            });
        }
        function endsAtChanged() {
            console.log("endsAt changed");
            window.axios.patch('/talks/{{ $talk->id }}', {
                ends_at: document.getElementById('ends_at').value
            }).then((response) => {
            });
        }
        function slidesChanged() {
            window.axios.patch('/talks/{{ $talk->id }}', {
                slides_url: document.getElementById('slides_url').value
            }).then((response) => {
            });
        }
        var contentsChanged = debounce(function () {
            window.axios.patch('/talks/{{ $talk->id }}', {
                contents: document.getElementById('contents').value
            }).then((response) => {
            });
        }, 750, false);

        function debounce(func, wait, immediate) {
            var timeout;

            return function executedFunction() {
                var context = this;
                var args = arguments;

                var later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };

                var callNow = immediate && !timeout;

                clearTimeout(timeout);

                timeout = setTimeout(later, wait);

                if (callNow) func.apply(context, args);
            }
        }

        document.getElementById('title').onblur = titleChanged;
        document.getElementById('presenter').onchange = presenterChanged;
        document.getElementById('starts_at').onchange = startsAtChanged;
        document.getElementById('ends_at').onchange = endsAtChanged;
        document.getElementById('slides_url').onchange = slidesChanged;
        document.getElementById('contents').onkeyup = contentsChanged;
    </script>
@endpush