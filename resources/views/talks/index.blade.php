@extends('layouts.app')

@section('content')
    <div class="container-md mx-auto max-w-2xl w-full bg-light-shades">
        @foreach ($talks as $talk)
            <div class="bg-white rounded mb-8 shadow-md">
                <div class="flex items-center">
                    <div class="text-left p-4 bg-white w-24">
                        <img class="rounded-full shadow-lg"
                             src="https://avatars.io/twitter/{{ $talk->presenter->twitter }}/small" alt="{{ $talk->presenter->twitter }}"/>
                    </div>
                    <div class="text-left p-4 bg-white w-32">
                        {{ $talk->presenter->name }}
                    </div>
                    <div class="text-left p-4 bg-white">
                        <a class="text-main-brand font-bold" href="{{ route('talks.edit', [$talk]) }}">
                            {{ $talk->title }}
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection