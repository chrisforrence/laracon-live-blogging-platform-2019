<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
</head>
<body class="bg-light-shades">
<!-- Header -->
<nav class="shadow-sm w-full bg-dark-shades h-24 mb-8">
    <div class="container mx-auto text-light-shades flex flex-row items-center justify-between h-full px-4">
        <div class="w-full">
            <a href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>
        <div>
            <a class="text-main-brand hover:underline" href="{{ route('talks.index') }}">Talks</a>
        </div>
    </div>
</nav>
<main class="container mx-auto bg-light-shades text-dark-shades px-4">
    @yield('content')
</main>
<footer class="mt-32 border-t border-dark-accent text-light-accent w-full">
    <div class="container mx-auto text-center pt-4">
        <p>
        Site by <a class="text-main-brand hover:underline" href="https://chrisforrence.com">Chris Forrence</a>.
        <a class="text-main-brand hover:underline" href="https://laravel.com">Laravel</a> is a copyright of Laravel LLC. All rights reserved.
        </p>
    </div>
</footer>

<script src="{{ asset('js/app.js') }}"></script>
@stack('scripts')
<!-- Fathom - simple website analytics - https://usefathom.com -->
<script>
    (function (f, a, t, h, o, m) {
        a[h] = a[h] || function () {
            (a[h].q = a[h].q || []).push(arguments)
        };
        o = f.createElement('script'),
            m = f.getElementsByTagName('script')[0];
        o.async = 1;
        o.src = t;
        o.id = 'fathom-script';
        m.parentNode.insertBefore(o, m)
    })(document, window, '//cdn.usefathom.com/tracker.js', 'fathom');
    fathom('set', 'siteId', 'MAWWOGET');
    fathom('trackPageview');
</script>
<!-- / Fathom -->
</body>
</html>
