@extends('layouts.site')

@section('content')
    <header class="flex flex-col md:flex-row items-center justify-between">
        <div class="text-3xl" id="title">{{ $talk->title }}</div>
        <div class="text-xl uppercase tracking-wider" id="presenter">{{ $talk->presenter->name }}</div>
    </header>
    <p class="italic text-center md:text-left {{ empty($talk->slides_url) ? 'hidden' : '' }}" id="slides">
        Slides: <a href="{{ $talk->slides_url }}" id="slides_url">{{ $talk->slides_url }}</a>
    </p>
    <article class="mt-8 markdown" id="contents">
        {!! app(Parsedown::class)->setSafeMode(true)->text($talk->contents) !!}
    </article>
@endsection