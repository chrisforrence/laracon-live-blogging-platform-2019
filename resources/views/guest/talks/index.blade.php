@extends('layouts.site')

@section('content')
    <div class="container-md mx-auto max-w-3xl w-full bg-light-shades">
        @foreach ($talks as $talk)
            <div class="{{ $talk->isCurrent() ? 'border-l-8 border-main-brand' : '' }} bg-white rounded mb-8 shadow-md">
                <div class="flex flex-col md:flex-row items-center">
                    <div class="text-left md:p-4 {{ $talk->isCurrent() ? 'md:pl-2' : '' }} md:w-24">
                        <a href="https://twitter.com/{{ $talk->presenter->twitter }}">
                            <img class="rounded-full" style="box-shadow: inset 0 2px 4px 0 hsla(0, 0%, 0%, 0.2)"
                                 src="https://avatars.io/twitter/{{ $talk->presenter->twitter }}/small" alt="{{ $talk->presenter->twitter }}"/>
                        </a>
                    </div>
                    <div class="text-left md:p-4 {{ $talk->isCurrent() ? 'md:pl-2' : '' }} md:w-48">
                        {{ $talk->presenter->name }}
                    </div>
                    <div class="text-left md:p-4 {{ $talk->isCurrent() ? 'md:pl-2' : '' }}">
                        @if ($talk->isUpcoming())
                            <span class="italic cursor-default" title="Starting in {{ $talk->starts_at->diffForHumans([
                                'syntax' => \Carbon\CarbonInterface::DIFF_ABSOLUTE,
                                'parts' => 3,
                                'join' => true,
                            ]) }}">{{ $talk->title }}</span>
                        @else
                            <a class="text-main-brand hover:underline" href="{{ route('talks.show', [$talk]) }}">{{ $talk->title }}</a>
                        @endif
                    </div>
                </div>
                <div class="pin-t"></div>
            </div>
        @endforeach
    </div>
@endsection