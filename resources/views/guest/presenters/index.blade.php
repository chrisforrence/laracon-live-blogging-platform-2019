@extends('layouts.site')
@section('content')
    <div class="flex flex-wrap -mx-4 overflow-hidden">
        @forelse ($presenters as $presenter)
            <div class="my-4 px-4 w-full md:w-1/2 xl:w-1/3">
                <div class="rounded bg-white">
                    <a href="https://twitter.com/{{ $presenter->twitter }}">
                        <img class="mx-auto overflow-hidden relative h-24 rounded shadow-lg " src="https://avatars.io/twitter/{{ $presenter->twitter }}/medium" alt="{{ $presenter->name }}"/></a>
                    <div class="text-center text-xl uppercase tracking-wider">{{ $presenter->name }}</div>
                </div>
            </div>
        @empty
            none
        @endforelse
    </div>
@endsection