@extends('layouts.site')

@section('content')
    <div>
        @unless (empty($upcoming))
            <p>
                <em>
                    Coming up:
                    {{ empty($upcoming->title) ? '' : ('"' . $upcoming->title . '" by ') }}
                    {{ $upcoming->presenter->name }}
                    in {{ $upcoming->starts_at->diffForHumans([
                        'syntax' => \Carbon\CarbonInterface::DIFF_ABSOLUTE,
                        'parts' => 3,
                        'join' => true,
                    ]) }}
                </em>
            </p>
        @endunless
        @unless (empty($talk))
            <header class="flex flex-col md:flex-row items-center justify-between">
                <div class="text-3xl" id="title">{{ $talk->title }}</div>
                <div class="text-xl uppercase tracking-wider" id="presenter">{{ $talk->presenter->name }}</div>
            </header>
            <p class="italic text-center md:text-left {{ empty($talk->slides_url) ? 'hidden' : '' }}" id="slides">
                Slides: <a href="{{ $talk->slides_url }}" id="slides_url">{{ $talk->slides_url }}</a>
            </p>
            <article class="mt-8 markdown" id="contents">
                {!! app(Parsedown::class)->setSafeMode(true)->text($talk->contents) !!}
            </article>
        @else
            No Current Talk
        @endunless
    </div>
@endsection

@push('scripts')
    <script>
        var contents = '';
        window.Echo.channel('home')
            .listen('TalkStarted', function (e) {
                window.location.reload();
            });
        @if ($talk)
        window.Echo.channel('talk.{{ $talk->id }}')
            .listen('TalkUpdated', function (e) {
                if (document.getElementById('title').innerText !== e.talk.title) {
                    document.getElementById('title').innerText = e.talk.title;
                }
                if (document.getElementById('presenter').innerText !== e.talk.presenter.name) {
                    document.getElementById('presenter').innerText = e.talk.presenter.name;
                }
                if (document.getElementById('slides_url').innerText !== e.talk.slides_url) {
                    document.getElementById('slides_url').innerText = e.talk.slides_url;
                    document.getElementById('slides_url').href = e.talk.slides_url;
                    document.getElementById('slides').classList.toggle('hidden', e.talk.slides_url === null || e.talk.slides_url === '');
                }
                if (contents !== e.talk.contents) {
                    contents = e.talk.contents;
                    document.getElementById('contents').innerHTML = window.markdown.render(contents);
                }
            });
        @endif
        function updateCurrentTalk() { }
        function updateMarkdown() { }
    </script>
@endpush