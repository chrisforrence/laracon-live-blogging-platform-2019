@extends('layouts.site')

@section('content')
<div class="w-full max-w-sm mx-auto">
    <form method="POST" action="{{ route('login') }}">
        @csrf
    <div class="md:flex md:items-center mb-6">
        <div class="md:w-1/3">
            <label for="email" class="block text-dark-shades font-bold md:text-right mb-1 md:mb-0 pr-4">
                {{ __('Email Address') }}
            </label>
        </div>
        <div class="md:w-2/3">
            <input type="email" id="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
                   class="bg-light-shades appearance-none border-2 border-dark-accent rounded w-full py-2 px-4 text-dark-accent leading-tight focus:outline-none focus:bg-white">
        </div>
    </div>

    <div class="md:flex md:items-center mb-6">
        <div class="md:w-1/3">
            <label for="password" class="block text-dark-shades font-bold md:text-right mb-1 md:mb-0 pr-4">
                {{ __('Password') }}
            </label>
        </div>
        <div class="md:w-2/3">
            <input type="password" id="password" name="password" value="" required autofocus
                   class="bg-light-shades appearance-none border-2 border-dark-accent rounded w-full py-2 px-4 text-dark-accent leading-tight focus:outline-none focus:bg-white">
        </div>
    </div>

        <div class="md:flex md:items-center mb-6">
            <div class="md:w-1/3"></div>
            <label class="md:w-2/3 block text-gray-500 font-bold">

                <input class="mr-2 leading-tight" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <span class="text-sm">
        {{ __('Remember Me') }}
      </span>
            </label>
        </div>

        <div class="md:flex md:items-center">
            <div class="md:w-1/3"></div>
            <div class="md:w-2/3">
                <button type="submit" class="shadow bg-dark-accent text-light-shades focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="button">
                    {{ __('Log In') }}
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
