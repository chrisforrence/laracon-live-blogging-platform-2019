<?php

use App\Presenter;
use Illuminate\Database\Seeder;

class SeedPresenters extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Presenter::create(['name' => 'Taylor Otwell', 'title' => 'Creator of Laravel', 'twitter' => '@taylorotwell']);
        Presenter::create(['name' => 'Evan You', 'title' => 'Creator of Vue.js', 'twitter' => '@youyuxi']);
        Presenter::create(['name' => 'Adam Wathan', 'title' => 'Creator of Tailwind CSS', 'twitter' => '@adamwathan']);
        Presenter::create(['name' => 'Matt Stauffer', 'title' => 'Partner at Tighten', 'twitter' => '@stauffermatt']);
        Presenter::create(['name' => 'Freek Van der Herten', 'title' => 'Partner at Spatie', 'twitter' => '@freekmurze']);
        Presenter::create(['name' => 'Katerina Trajchevska', 'title' => 'CEO at Adeva', 'twitter' => '@ktrajchevska']);
        Presenter::create(['name' => 'Jason McCreary', 'title' => 'Creator of Laravel Shift', 'twitter' => '@gonedark']);
        Presenter::create(['name' => 'Justin Jackson', 'title' => 'Co-Founder at Transistor.fm', 'twitter' => '@mijustin']);
        Presenter::create(['name' => 'Steven Schoger', 'title' => 'Designer at Laravel', 'twitter' => '@steveschoger']);
        Presenter::create(['name' => 'Kaya Thomas', 'title' => 'Developer at Slack', 'twitter' => '@kthomas901']);
        Presenter::create(['name' => 'Caleb Porzio', 'title' => '🔥Hot Tip King', 'twitter' => '@calebporzio']);
        Presenter::create(['name' => 'Bobby Bouwmann', 'title' => 'Creator of MarkdownMail', 'twitter' => '@bobbybouwmann']);
        Presenter::create(['name' => 'Christoph Rumpel', 'title' => 'Creator of Laravel Core Adventures', 'twitter' => '@christophrumpel']);
        Presenter::create(['name' => 'Colin Decarlo', 'title' => 'Developer at Vehikl', 'twitter' => '@colindecarlo']);
        Presenter::create(['name' => 'Marcel Pociot', 'title' => 'Partner at Beyond Code', 'twitter' => '@marcelpociot']);
        Presenter::create(['name' => 'Keith Damiani', 'title' => 'Senior Dev at Tighten', 'twitter' => '@keithdamiani']);
        Presenter::create(['name' => 'Jonathan Reinink', 'title' => 'Coauthor of TailwindCSS', 'twitter' => '@reinink']);
        Presenter::create(['name' => 'Dries Vints', 'title' => 'Developer, Laravel', 'twitter' => '@driesvints']);
    }
}
