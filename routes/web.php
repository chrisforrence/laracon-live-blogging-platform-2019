<?php
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'DashboardController@index')->name('home');
    Route::resource('presenters', 'PresenterController');
    Route::resource('talks', 'TalkController');
});

Route::group(['middleware' => ['guest']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('presenters', 'PresenterController')->only(['index', 'show']);
    Route::resource('talks', 'TalkController')->only(['index', 'show']);
});

Auth::routes(['register' => false, 'reset' => false, 'verify' => false]);
Broadcast::routes();