<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('home', \App\Broadcasting\HomeChannel::class);
Broadcast::channel('talk.{talk}', \App\Broadcasting\TalkChannel::class);